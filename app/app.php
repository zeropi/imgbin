<?php

use Silex\Application;

use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;

use Silex\Provider\TranslationServiceProvider;

// var_dump(__DIR__.'/../templates');

$app = new Application();
$app->register(new UrlGeneratorServiceProvider());
$app->register(new ValidatorServiceProvider());
$app->register(new ServiceControllerServiceProvider());

$app->register(new TranslationServiceProvider(), array(
	'locale' => 'en',
));
$app['translator'] = $app->share($app->extend('translator', function($translator, $app) {

    return $translator;
}));

$app->register(new TwigServiceProvider(), array(
    'twig.path' => array(__DIR__.'/../app/views'),
    'twig.options' => array(
    	'cache' => __DIR__.'/../cache/twig',
    	'debug' => TRUE, // TODO
    ),
));
$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
    // add custom globals, filters, tags, ...
	$twig->addGlobal('meta', array(
		'locale' => $app['translator']->getLocale(),
	));
	$twig->addGlobal('home_url', $app['url_generator']->generate('homepage'));
	$twig->addGlobal('about_url', $app['url_generator']->generate('about'));
    return $twig;
}));

return $app;