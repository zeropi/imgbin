<?php

// Routes

// $app->match('/', function () {
//    return "home";
// });

use Silex\Provider\FormServiceProvider;	

// use Symfony\Component\Form\FormError;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

// use Symfony\Component\HttpFoundation\File;
// use Symfony\Component\HttpFoundation\File\UploadedFile;


$app->register(new FormServiceProvider());

// use Symfony\Component\Form\FormError;



/**
 * Home page, with form
 */
// $app->match('/', function () use ($app) {
$app->match('/', function (Request $request) use ($app) {

	$message = FALSE;

	// Create form
	$form = $app['form.factory']->createBuilder('form')
		->add('file', 'file', array(
			'label' => FALSE,
		))
        ->add('remove_metadata', 'checkbox', array(
            'label' => "Remove metadata",
            'required' => FALSE,
            'data' => TRUE, // Default to true
        ))
        ->add('submit', 'submit', array(

        ))
        ->getForm()
    ;

    // $request = $app['request'];
    if ($request->isMethod('POST')) {

    	$form->handleRequest($request);
        if ($form->isValid()) {

            // Get form data
            $data = $form->getData();
            $file = $data['file'];
            $remove_metadata = $data['remove_metadata'];

            // User message
            $message = array(
                'text' => "File upload succesfully.",
                'status' => 'success'
            );

            // Prepare file destination
			$dir = __DIR__ . '/../web/files/';
			$filename = time() . "_" .rand(1, 99999);

     		// Guess the extension (more secure)
			$extension = $file->guessClientExtension();
            if (!$extension) {
                // extension cannot be guessed
                $extension = 'bin';
            }

            // Remove metadata from file after upload
            if ($remove_metadata) {
                if ($extension == 'jpg' || $extension == 'jpg') {
                    // Strip out metadata
                    $res = imagecreatefromjpeg($file->getRealPath());
                    imagejpeg($res, $dir . $filename.'.'.$extension, 100);
                }
                elseif ($extension == 'png') {
                    // Strip out metadata
                    $res = imagecreatefrompng($file->getRealPath());
                    imagepng($res, $dir . $filename.'.'.$extension, 0);
                }
                else {
                    $file->move($dir, $filename.'.'.$extension);
                }
            }
            else {
                $file->move($dir, $filename.'.'.$extension);
            }

			$url = $app['url_generator']->generate('homepage') . "files/".$filename.'.'.$extension;

        } else {
            // Error message
        	$message = array(
     			'text' => "File upload failed.",
     			'status' => 'error'
     		);
            // $form->addError(new FormError('This is a global error'));
        }
    }

    return $app['twig']->render('index.html.twig', array(
    	'form'  => $form->createView(),
    	'message' => $message,
    	'url' => (!empty($url)) ? $url : FALSE
    ));
})
->bind('homepage')
;


/**
 * About page
 */
$app->match('/about', function (Request $request) use ($app) {
    return $app['twig']->render('about.html.twig', array(
    ));
})
->bind('about')
;