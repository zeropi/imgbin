
1. Download dependencies with composer:


  curl -sS https://getcomposer.org/installer | php
  php composer.phar install


2. Create /cache directory, make it writable

  mkdir cache && chmod 777 cache


2. Create /web/files directory, make it writable

  mkdir web/files && chmod 777 web/files