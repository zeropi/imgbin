<?php

// web/index.php

require_once __DIR__.'/../vendor/autoload.php';

require_once __DIR__.'/../app/app.php';

require_once __DIR__.'/../app/config/local.php';
require_once __DIR__.'/../app/controllers.php';


$app->run();